package com.ykms.monzyourhealthnetwork;

import java.util.ArrayList;

import org.w3c.dom.Text;

import android.app.ActionBar;
import android.app.Activity;
import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.nhaarman.listviewanimations.appearance.simple.SwingBottomInAnimationAdapter;
import com.ykms.monzyourhealthnetwork.adapter.CategoryListAdapter;
import com.ykms.monzyourhealthnetwork.model.Category;
import com.ykms.monzyourhealthnetwork.utils.DBHandler;

public class SearchResultsActivity extends Activity {

	TextView tvSearchResult;
	ListView lv;
	DBHandler db = new DBHandler(this);

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search_result);

		ActionBar ab = getActionBar();
		ab.setBackgroundDrawable(getResources().getDrawable(
				R.color.primary_color_dark));

		lv = (ListView) findViewById(R.id.lvSearchResult);
		tvSearchResult	=(TextView) findViewById(R.id.tvSearchResult);

		handleIntent(getIntent());
	}

	@Override
	protected void onNewIntent(Intent intent) {
		setIntent(intent);
		handleIntent(intent);
	}

	/**
	 * Handling intent data
	 */
	private void handleIntent(Intent intent) {
		if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
			String query = intent.getStringExtra(SearchManager.QUERY);

			/**
			 * Use this query to display search results like 1. Getting the data
			 * from SQLite and showing in listview 2. Making webrequest and
			 * displaying the data For now we just display the query only
			 */

			ArrayList<Category> categoryList = db.getCategoryByKey(query);
			
			if(categoryList==null)
			{
				tvSearchResult.setText("No Category found");
			}
			
			CategoryListAdapter adapter = new CategoryListAdapter(this,
					R.layout.list_item_category, categoryList);

			// third party libs
			SwingBottomInAnimationAdapter swingBottomInAnimationAdapter = new SwingBottomInAnimationAdapter(
					(BaseAdapter) adapter);
			swingBottomInAnimationAdapter.setAbsListView(lv);

			assert swingBottomInAnimationAdapter.getViewAnimator() != null;
			swingBottomInAnimationAdapter.getViewAnimator()
					.setInitialDelayMillis(500);

			lv.setAdapter(swingBottomInAnimationAdapter);

			lv.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View view,
						int arg2, long arg3) {
					// TODO Auto-generated method stub
					TextView categoryId = (TextView) view
							.findViewById(R.id.tvCategoryId);
					Intent i = new Intent(SearchResultsActivity.this,
							MainActivity.class);
					i.putExtra("categoryId", categoryId.getText());
					startActivity(i);
				}

			});

		}

	}

}
