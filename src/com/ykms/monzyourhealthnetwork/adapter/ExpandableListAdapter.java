package com.ykms.monzyourhealthnetwork.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.ykms.monzyourhealthnetwork.R;
import com.ykms.monzyourhealthnetwork.model.Child;
import com.ykms.monzyourhealthnetwork.model.Children;
import com.ykms.monzyourhealthnetwork.model.Parent;

public class ExpandableListAdapter extends BaseExpandableListAdapter {

	private Context _context;
	private ArrayList<Parent> _listDataHeader; // header titles

	// child data in format of header title, child title
	// private HashMap<String, List<String>> _listDataChild;

	public ExpandableListAdapter(Context context,
			ArrayList<Parent> listDataHeader) {
		this._context = context;
		this._listDataHeader = listDataHeader;

	}

	@Override
	public Object getChild(int groupPosition, int childPosititon) {
		// TODO Auto-generated method stub
		return _listDataHeader.get(groupPosition).getChildren()
				.get(childPosititon);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return childPosition;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		//final String childText = (String) getChild(groupPosition, childPosition);

		Parent parentss = _listDataHeader.get(groupPosition);
		final Child child = parentss.getChildren().get(childPosition);

		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) this._context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.list_item, null);
		}

		TextView tvChief = (TextView) convertView
				.findViewById(R.id.tvChildChief);
		TextView tvAddress = (TextView) convertView
				.findViewById(R.id.tvChildAddress);

		TextView tvcity = (TextView) convertView
				.findViewById(R.id.tvChildCity);

		TextView tvState = (TextView) convertView
				.findViewById(R.id.tvChildState);

		TextView tvZip = (TextView) convertView
				.findViewById(R.id.tvChildZip);
		
		TextView tvPhone = (TextView) convertView
				.findViewById(R.id.tvChildPhone);
		
		TextView tvWebsite = (TextView) convertView
				.findViewById(R.id.tvChildWebsite);
		TextView tvEmail = (TextView) convertView
				.findViewById(R.id.tvChildEmail);



		tvAddress.setText(child.getAddress());
		tvChief.setText(child.getChiefPractitioner());
		tvcity.setText(child.getCity());
		tvEmail.setText(child.getEmail());
		tvPhone.setText(child.getPhone());
		tvState.setText(child.getState());
		tvWebsite.setText(child.getWebsite());
		tvZip.setText(child.getZipCode());
		return convertView;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		// TODO Auto-generated method stub

		 int size=0;
         if(_listDataHeader.get(groupPosition).getChildren()!=null)
             size = _listDataHeader.get(groupPosition).getChildren().size();
         return size;
	}

	@Override
	public Object getGroup(int groupPosition) {
		// TODO Auto-generated method stub
		return this._listDataHeader.get(groupPosition);
	}

	@Override
	public int getGroupCount() {
		// TODO Auto-generated method stub
		return this._listDataHeader.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		// TODO Auto-generated method stub
		return groupPosition;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		Parent parentss = _listDataHeader.get(groupPosition);

		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) this._context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.list_group, null);
		}

		TextView tvOfcName = (TextView) convertView
				.findViewById(R.id.tvDetailOfcName);
		TextView tvCity = (TextView) convertView
				.findViewById(R.id.tvDetailCity);
		TextView tvNotes = (TextView) convertView
				.findViewById(R.id.tvDetailNotes);

		tvOfcName.setText(parentss.getOfficeName().toString());
		tvCity.setText(parentss.getCity().toString());
		tvNotes.setText(parentss.getNotes().toString());

		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return true;
	}

}
