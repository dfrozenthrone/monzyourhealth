package com.ykms.monzyourhealthnetwork.adapter;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.ykms.monzyourhealthnetwork.model.Category;

public class CategoryListAdapter extends ArrayAdapter<Category> {
	private Activity activity;
	
	LayoutInflater vi;
	int Resource;
	ViewHolder holder;
	ArrayList<Category> categoryList;
	

	// public ImageView imageview;
	// public TextView tName, tDate;

	public CategoryListAdapter(Context context, int resource,
			ArrayList<Category> objects) {
		super(context, resource, objects);
		// Log.d("WHERE M I3???????", "U R HERE!!!!!!!");
		vi = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		Resource = resource;
		categoryList =objects;
	
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// convert view = design
		View v = convertView;
		if (v == null) {
			holder = new ViewHolder();
			v = vi.inflate(Resource, null);
			
			holder.tId	= (TextView) v.findViewById(com.ykms.monzyourhealthnetwork.R.id.tvCategoryId);
			
			holder.tTitle = (TextView) v.findViewById(com.ykms.monzyourhealthnetwork.R.id.tvCategorytitle);
			

			v.setTag(holder);
		} else {
			holder = (ViewHolder) v.getTag();
		}

		//Log.d("ITEM NAME:::::::::", eventsList.get(position).get("auction_id"));

		Log.d("categoryIDDD", categoryList.get(position).getId().toString());
		holder.tId.setText(categoryList.get(position)
				.getId().toString());
		holder.tTitle.setText(categoryList.get(position).getCategoryName().toString());
		
		return v;

	}

	static class ViewHolder {
		
		public TextView tId;
		public TextView tTitle;
		

	}

	
}
