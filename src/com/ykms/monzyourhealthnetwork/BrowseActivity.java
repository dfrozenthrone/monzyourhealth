package com.ykms.monzyourhealthnetwork;

import java.util.ArrayList;

import com.nhaarman.listviewanimations.appearance.simple.SwingBottomInAnimationAdapter;
import com.ykms.monzyourhealthnetwork.adapter.CategoryListAdapter;
import com.ykms.monzyourhealthnetwork.model.Category;
import com.ykms.monzyourhealthnetwork.utils.DBHandler;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class BrowseActivity extends Activity {

	ListView lvCategory;

	String[] categories = { "Allergists", "Cardiologists", "Chiropractors",
			"Allergists", "Cardiologists", "Chiropractors", "Allergists",
			"Cardiologists", "Chiropractors", "Allergists", "Cardiologists",
			"Chiropractors" };

	DBHandler db = new DBHandler(this);

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_browse);

		ActionBar ab = getActionBar();
		ab.setBackgroundDrawable(getResources().getDrawable(
				R.color.primary_color_dark));
		ab.setHomeButtonEnabled(true);

		intializeUI();
		//insertCategory();
		
		ArrayList<Category> categoryList =  db.getAllCategory();

		CategoryListAdapter adapter = new CategoryListAdapter(this, R.layout.list_item_category, categoryList);

		//ArrayAdapter<Category> adapter = new ArrayAdapter<Category>(this, R.layout.list_item_category	, R.id.tvCategorytitle	, categoryList);
		
		
		//thirdparty libs for animation
		
		SwingBottomInAnimationAdapter swingBottomInAnimationAdapter = new SwingBottomInAnimationAdapter(
				(BaseAdapter) adapter);
		swingBottomInAnimationAdapter.setAbsListView(lvCategory);

		assert swingBottomInAnimationAdapter.getViewAnimator() != null;
		swingBottomInAnimationAdapter.getViewAnimator().setInitialDelayMillis(
				500);
		
		
		
		lvCategory.setAdapter(swingBottomInAnimationAdapter);

		lvCategory.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View view, int position,
					long arg3) {
				// TODO Auto-generated method stub
				
				 
				TextView categoryId = (TextView) view.findViewById(R.id.tvCategoryId);
				Intent i = new Intent(BrowseActivity.this, MainActivity.class);
				i.putExtra("categoryId", categoryId.getText());
				startActivity(i);
			}
		});

	}

	private void intializeUI() {

		lvCategory = (ListView) findViewById(R.id.categoryListView);

	}
	
	private void insertCategory() {
		
		db.addCategory(new Category(1, "Allergists"));
		db.addCategory(new Category(2, "Cardiologists"));
		db.addCategory(new Category(3, "Chiropractors"));
		db.addCategory(new Category(4, "Community Clinics"));
		db.addCategory(new Category(5, "Counselors"));
		db.addCategory(new Category(6, "Dentists"));
		db.addCategory(new Category(7, "Dermatologists"));
		db.addCategory(new Category(8, "Dietitians/Nutritionists"));
		db.addCategory(new Category(9, "ENT Specialists"));
		db.addCategory(new Category(10, "ENT Specialists"));

	

}

}
