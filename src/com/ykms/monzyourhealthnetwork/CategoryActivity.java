package com.ykms.monzyourhealthnetwork;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;

public class CategoryActivity extends Activity {

	ImageButton btnBrowse, btnSearch;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_category);
		
		ActionBar ab = getActionBar();
		ab.setBackgroundDrawable(getResources().getDrawable(R.color.primary_color_dark));
		ab.hide();
		
		
		intializeUI();
		
		btnBrowse.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				Intent i = new Intent(CategoryActivity.this, BrowseActivity.class);
				startActivity(i);
			}
		});
		
		
		btnSearch.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(CategoryActivity.this, SearchActivity.class);
				startActivity(i);
			}
		});
		
	
	}

	private void intializeUI() {
		btnBrowse = (ImageButton) findViewById(R.id.imageButtonBrowse);
		btnSearch = (ImageButton) findViewById(R.id.imageButtonSearch);
	}

}
