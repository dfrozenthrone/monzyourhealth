package com.ykms.monzyourhealthnetwork.utils;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import com.ykms.monzyourhealthnetwork.model.Category;
import com.ykms.monzyourhealthnetwork.model.Child;
import com.ykms.monzyourhealthnetwork.model.Parent;

public class DBHandler extends SQLiteOpenHelper {

	private static String DATABASE_NAME = "db_monzyour";
	private static int DATABASE_VERSION = 2;
	private static String TABLE_NAME_ENTITY = "entity";
	private static String TABLE_NAME_CATEGORY = "category";
	private static String TAG_ID = "id";
	private static String TAG_IMAGE = "image_logo";
	private static String TAG_OFFICE_NAME = "office_name";
	private static String TAG_ADDRESS = "address";
	private static String TAG_STATE = "state";
	private static String TAG_CHIEF_PRACTITIONER = "chief_practitioner";
	private static String TAG_CITY = "city";
	private static String TAG_ZIP_CODE = "zip_code";
	private static String TAG_PHONE = "phone";
	private static String TAG_NOTES = "notes";
	private static String TAG_WEBSITE = "website";
	private static String TAG_EMAIL = "email";
	private static String TAG_IS_PREMIUM = "is_premium";
	private static String TAG_SQL_ENTITY = "CREATE TABLE entity (id TEXT, category_id INTEGER, image_logo TEXT, office_name TEXT,chief_practitioner TEXT, address TEXT,city TEXT, state TEXT, zip_code TEXT,phone TEXT,notes TEXT, website TEXT , email TEXT , is_premium TEXT)";
	private static String TAG_SQL_CATEGORY = "CREATE TABLE category (id INTEGER, name TEXT)";

	public DBHandler(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		db.execSQL(TAG_SQL_ENTITY);
		db.execSQL(TAG_SQL_CATEGORY);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_CATEGORY);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_ENTITY);

		onCreate(db);
	}

	public void addEntity(Parent parent) {
		SQLiteDatabase db = this.getWritableDatabase();
		String sql = "INSERT INTO entity (id ,category_id, image_logo , office_name ,chief_practitioner , address ,city , state , zip_code ,phone ,notes , website  , email  , is_premium ) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		SQLiteStatement stmt = db.compileStatement(sql);

		stmt.bindString(1, parent.getId());
		stmt.bindLong(2, parent.getCategoryId());
		stmt.bindString(3, "imagelogo");
		stmt.bindString(4, parent.getOfficeName());
		stmt.bindString(5, parent.getChildren().get(0).getChiefPractitioner());
		stmt.bindString(6, parent.getChildren().get(0).getAddress());
		stmt.bindString(7, parent.getChildren().get(0).getCity());
		stmt.bindString(8, parent.getChildren().get(0).getState());
		stmt.bindString(9, parent.getChildren().get(0).getZipCode());
		stmt.bindString(10, parent.getChildren().get(0).getPhone());
		stmt.bindString(11, parent.getNotes());
		stmt.bindString(12, parent.getChildren().get(0).getWebsite());
		stmt.bindString(13, parent.getChildren().get(0).getEmail());
		stmt.bindString(14, "0");

		stmt.execute();
		db.close();

	}
	
	public void addCategory(Category category) {
		SQLiteDatabase db = this.getWritableDatabase();
		String sql = "INSERT INTO category (id,name) VALUES (?,?)";
		SQLiteStatement stmt = db.compileStatement(sql);

		stmt.bindString(1, category.getId().toString());
		stmt.bindString(2, category.getCategoryName());
		
		stmt.execute();
		db.close();

	}

	public ArrayList<Parent> getAllParent() {
		ArrayList<Parent> parentList = new ArrayList<Parent>();
		String query = "SELECT * FROM " + TABLE_NAME_ENTITY;
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(query, null);
		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Parent parent = new Parent();
				// student.setStudentId(Integer.parseInt(cursor.getString(0)));
				parent.setOfficeName(cursor.getString(2));
				parent.setId(cursor.getString(0));
				parent.setCity(cursor.getString(5));
				parent.setNotes(cursor.getString(9));
				parent.setChildren(new ArrayList<Child>());

				Child child = new Child();
				child.setAddress(cursor.getString(4));
				child.setChiefPractitioner(cursor.getString(3));
				child.setCity(cursor.getString(5));
				child.setState(cursor.getString(6));
				child.setZipCode(cursor.getString(7));
				child.setPhone(cursor.getString(8));
				child.setWebsite(cursor.getString(10));
				child.setEmail(cursor.getString(11));
				
				
				parent.getChildren().add(child);
				parentList.add(parent);

			} while (cursor.moveToNext());
		}
		db.close();
		return parentList;

	}
	
	public ArrayList<Parent> getAllParentByCategory(Integer categoryId) {
		ArrayList<Parent> parentList = new ArrayList<Parent>();
		String query = "SELECT * FROM " + TABLE_NAME_ENTITY +" WHERE category_id="+ categoryId;
		SQLiteDatabase db = this.getWritableDatabase();
		Log.d("query", query);
		Cursor cursor = db.rawQuery(query, null);
		
		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Parent parent = new Parent();
				// student.setStudentId(Integer.parseInt(cursor.getString(0)));
				
				parent.setId(cursor.getString(0));
				parent.setOfficeName(cursor.getString(3));
				parent.setCity(cursor.getString(6));
				parent.setNotes(cursor.getString(10));
				parent.setChildren(new ArrayList<Child>());

				Child child = new Child();
				child.setAddress(cursor.getString(5));
				child.setChiefPractitioner(cursor.getString(4));
				child.setCity(cursor.getString(6));
				child.setState(cursor.getString(7));
				child.setZipCode(cursor.getString(8));
				child.setPhone(cursor.getString(9));
				child.setWebsite(cursor.getString(11));
				child.setEmail(cursor.getString(12));
				
				
				parent.getChildren().add(child);
				parentList.add(parent);

			} while (cursor.moveToNext());
		}
		db.close();
		Log.d("parentlist", parentList.toString());
		return parentList;
		

	}
	
	public ArrayList<Category> getAllCategory() {
		ArrayList<Category> categoryList = new ArrayList<Category>();
		String query = "SELECT * FROM " + TABLE_NAME_CATEGORY;
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(query, null);
		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Category category = new Category();
				category.setId(cursor.getInt(0));
				category.setCategoryName(cursor.getString(1));
				categoryList.add(category);

			} while (cursor.moveToNext());
		}
		db.close();
		Log.d("categoryList", categoryList.toString());
		return categoryList;

	}
	
	public ArrayList<Category> getCategoryByKey(String key) {
		ArrayList<Category> categoryList = new ArrayList<Category>();
		String query = "SELECT * FROM " + TABLE_NAME_CATEGORY +" WHERE name LIKE "+"\'%"+key+"%\'" ;
		

		Log.d("Searchquery", query);
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(query, null);
		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Category category = new Category();
				category.setId(cursor.getInt(0));
				category.setCategoryName(cursor.getString(1));
				categoryList.add(category);
			} while (cursor.moveToNext());
		}
		db.close();
		Log.d("categoryList", categoryList.toString());
		return categoryList;

	}

}
