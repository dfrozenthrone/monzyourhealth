package com.ykms.monzyourhealthnetwork.model;

public class Child {

	String chiefPractitioner;
	String address;
	String city;
	String state;
	String zipCode;
	String phone;
	
	String website;
	String email;
	//boolean isPremium;

	public String getChiefPractitioner() {
		return chiefPractitioner;
	}

	public void setChiefPractitioner(String chiefPractitioner) {
		this.chiefPractitioner = chiefPractitioner;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}


	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	

}
