package com.ykms.monzyourhealthnetwork.model;

import java.util.ArrayList;

public class Parent {

	String id;
	String officeName;
	String city;
	String notes;
	
	ArrayList<Child> children;
	Integer categoryId;

	public Integer getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}

	public Parent(String id, String officeName, String city, String notes, Integer categoryId,
			ArrayList<Child> children) {
		this.id = id;

		this.officeName = officeName;
		this.city = city;
		this.notes = notes;
		this.children = children;
		this.categoryId= categoryId;
	}

	public Parent() {

	}

	public ArrayList<Child> getChildren() {
		return children;
	}

	public void setChildren(ArrayList<Child> children) {
		this.children = children;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOfficeName() {
		return officeName;
	}

	public void setOfficeName(String officeName) {
		this.officeName = officeName;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

}
