package com.ykms.monzyourhealthnetwork;

import java.util.ArrayList;

import android.app.ActionBar;
import android.app.Activity;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;

import com.ykms.monzyourhealthnetwork.model.Category;
import com.ykms.monzyourhealthnetwork.model.Child;
import com.ykms.monzyourhealthnetwork.model.Children;
import com.ykms.monzyourhealthnetwork.model.Parent;
import com.ykms.monzyourhealthnetwork.utils.DBHandler;

public class MainActivity extends Activity {

	ExpandableListAdapter listAdapter;
	ExpandableListView expListView;
	ArrayList<Parent> listDataHeader;
	DBHandler db = new DBHandler(this);

	// HashMap<String, List<String>> listDataChild;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_activity);

		ActionBar ab = getActionBar();
		ab.setBackgroundDrawable(getResources().getDrawable(
				R.color.primary_color_dark));
		ab.setHomeButtonEnabled(true);

		// SQLiteDatabase db = new DBHandler(this).getWritableDatabase();

		// insert();
		
		String categoryId = getIntent().getStringExtra("categoryId");
		Integer categoryIds  = Integer.parseInt(categoryId) ;
		
		Log.d("categoryId",categoryIds.toString());

		
		ArrayList<Parent> parentslists = db.getAllParentByCategory(categoryIds);

		
		expListView = (ExpandableListView) findViewById(R.id.expListView);

		ArrayList<Parent> datas = prepareListData();

		listAdapter = new com.ykms.monzyourhealthnetwork.adapter.ExpandableListAdapter(
				this, parentslists);

		expListView.setAdapter(listAdapter);

	}

	

	private void insert() {

		for (int i = 0; i <= 30; i++) {

			Child ch = new Child();
			ArrayList<Child> childList = new ArrayList<Child>();
			ch.setAddress("address");
			ch.setChiefPractitioner("Dr manish m");
			ch.setCity("ktm");
			ch.setEmail("dfrozenthrone@gmail.com");
			ch.setPhone("9843677237");
			ch.setState("Bagmati");
			ch.setWebsite("www.manishm.com.np");
			ch.setZipCode("44710");
			childList.add(ch);

			db.addEntity(new Parent("1", "officeName", "city",
					"notesnotesnostnso",1, childList));
		}
	}

	private ArrayList<Parent> prepareListData() {
		/*
		 * listDataHeader = new ArrayList<String>();
		 * 
		 * 
		 * // Adding child data listDataHeader.add("Top 250");
		 * listDataHeader.add("Now Showing");
		 * listDataHeader.add("Coming Soon..");
		 */

		/*
		 * listDataChild = new HashMap<String, List<String>>();
		 * 
		 * // Adding child data List<String> top250 = new ArrayList<String>();
		 * top250.add("The Shawshank Redemption"); top250.add("The Godfather");
		 * top250.add("The Godfather: Part II"); top250.add("Pulp Fiction");
		 * top250.add("The Good, the Bad and the Ugly");
		 * 
		 * List<String> nowShowing = new ArrayList<String>();
		 * nowShowing.add("The Conjuring"); nowShowing.add("Despicable Me 2");
		 * nowShowing.add("Turbo"); nowShowing.add("Grown Ups 2");
		 * nowShowing.add("Red 2");
		 * 
		 * List<String> comingSoon = new ArrayList<String>();
		 * comingSoon.add("2 Guns"); comingSoon.add("The Smurfs 2");
		 * comingSoon.add("The Spectacular Now"); comingSoon.add("The Canyons");
		 * comingSoon.add("Europa Report");
		 */

		listDataHeader = new ArrayList<Parent>();

		for (int i = 0; i < 10; i++) {

			/*
			 * // Create parent class object Parent parent = new Parent();
			 * parent.setId("1"); parent.setOfficeName("Office" + i);
			 * parent.setCity("city " + i); parent.setNotes("notes" + i);
			 * parent.setChildren(new ArrayList<Child>());
			 * 
			 * // Create Child class object Child childs = new Child();
			 * childs.setAddress("address" + i);
			 * childs.setChiefPractitioner("chiefPractitioner" + i);
			 * childs.setCity("city" + i); childs.setEmail("email" + i);
			 * childs.setPhone("phone" + i); childs.setState("state");
			 * childs.setWebsite("website"); childs.setZipCode("zipCode"); //
			 * Add Child class object to parent class object
			 * parent.getChildren().add(childs);
			 * 
			 * listDataHeader.add(parent);
			 */

			// Create parent class object
			Parent parent = new Parent();
			parent.setId("7");
			parent.setOfficeName("Bingham Family Clinic");
			parent.setCity("Ann Arbor");
			parent.setNotes("the first line of notes � for premium listings only");
			parent.setChildren(new ArrayList<Child>());

			// Create Child class object
			Child childs = new Child();
			childs.setAddress("123 Main Street, Suite C");
			childs.setChiefPractitioner("Dr. Donald Bingham");
			childs.setCity("Ann Arbor");
			childs.setEmail("dfroethrone@gmail.com");
			childs.setPhone("9843677237");
			childs.setState("MN");
			childs.setWebsite("http://www.manishm.com.np");
			childs.setZipCode("48103");
			// Add Child class object to parent class object
			parent.getChildren().add(childs);

			listDataHeader.add(parent);
		}

		return listDataHeader;

	}

}
