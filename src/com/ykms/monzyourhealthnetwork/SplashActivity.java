package com.ykms.monzyourhealthnetwork;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;

public class SplashActivity extends Activity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);
		
		
		android.app.ActionBar ab = getActionBar();
		ab.hide();
		
		
		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				Intent i = new Intent(SplashActivity.this, CategoryActivity.class);
				startActivity(i);
				//overridePendingTransition(R.anim.slide_in_left,
						//R.anim.slide_out_left);

				finish();
			}
		}, 3000);
		
	}
	
	
	
	

}
